﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public float meleeDmg;
    public float meleeAttackTime;
    public float melee_CD;
    public GameObject AttackHitbox;
    public GameObject attackSkillHitbox;
    public GameObject shieldSkillHitbox;
    public float activeDmgFrames;
    public bool isAttacking;
    //kjOS
    public float comboTimeout = 1f;
    public int combocounter=0;
    public float comboMaxTime=1f;
    public float blockTime=1f;
    public bool isBlocking;
    public GameObject shieldHitbox;
    public bool autoAttack;
    public float comboAutoAttackTimer;
    //Atributos
    public float shieldRechargeRate;
    //STATS
    private Animator anim;
    private PlayerStats ps;
    private MovementInput mi;
    // Start is called before the first frame update
    void Start()
    {
        ps=GetComponent<PlayerStats>();
        mi = GetComponent<MovementInput>();
        anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        ComboTime();
        MoveSpeedRampUp();
        if (isAttacking && Input.GetKeyDown(KeyCode.Z))
            autoAttack = true;
        if (Input.GetKeyDown(KeyCode.Z) && Time.time > meleeAttackTime&& !isAttacking && !isBlocking)
        {
            activeDmgFrames = 1.3f;
            BasicAttack();
            meleeAttackTime = Time.time + melee_CD;
        }
        if (autoAttack&& !isAttacking)
        {
            BasicAttack();
            meleeAttackTime = Time.time + melee_CD;
            autoAttack = false;
        }
        if (Input.GetKeyDown(KeyCode.C) && !isAttacking)
        {
            isBlocking = true;
        }
        if (Input.GetKeyUp(KeyCode.C) && !isAttacking)
        {
            mi.Velocity = 0.2f;
            isBlocking = false;
            stop_shield(shieldHitbox.GetComponent<Collider>());
        }
        if (isBlocking)
        {
            ShieldBlock();
        }
        if(Input.GetKeyDown(KeyCode.J)&& ps.stamina >= 50&& !isAttacking && !isBlocking)
        {
            StartCoroutine(AttackSkill());
        }
        if (Input.GetKeyDown(KeyCode.K) && ps.stamina >= 50 && !isAttacking && !isBlocking)
        {
            StartCoroutine(AttackSkill2());
        }

        DecreaseComboTime();
        shieldLifeRecharge();


    }
    IEnumerator AttackSkill()
    {
        mi.Velocity = 0;
        ps.stamina -= 50;
        isAttacking = true;
        yield return new WaitForSeconds(.5f);
        

        

        Collider b_collider = attackSkillHitbox.GetComponent<Collider>();
        //basicAttackHitbox.GetComponent<MeleeBehavior>().meleeDmg = outputDamage;


        Debug.Log("attack begins");
        combocounter++;
        



        //ComboTime();


        comboTimeout = comboMaxTime;
        attackSkillHitbox.SetActive(true);
        b_collider = attackSkillHitbox.GetComponent<Collider>();
        b_collider.enabled = true;

       
        StartCoroutine(SpecialAttackOver(b_collider, 0.1f));
        //StartCoroutine(stop_BaseAttack(b_collider, activeDmgFrames));

    }
    IEnumerator AttackSkill2()
    {
        mi.Velocity = 0;
        ps.stamina -= 50;
        isAttacking = true;
        yield return new WaitForSeconds(.5f);




        Collider b_collider = shieldSkillHitbox.GetComponent<Collider>();
        //basicAttackHitbox.GetComponent<MeleeBehavior>().meleeDmg = outputDamage;


        Debug.Log("attack begins");
        combocounter++;




        //ComboTime();


        comboTimeout = comboMaxTime;
        shieldSkillHitbox.SetActive(true);
        b_collider = shieldSkillHitbox.GetComponent<Collider>();
        b_collider.enabled = true;


        StartCoroutine(SpecialAttackOver2(b_collider, 0.1f));
        //StartCoroutine(stop_BaseAttack(b_collider, activeDmgFrames));

    }
    IEnumerator SpecialAttackOver(Collider col, float delay)
    {
        yield return new WaitForSeconds(delay);
        col.enabled = false;
        attackSkillHitbox.SetActive(false);

     
      
        isAttacking = false;
      

    }
    IEnumerator SpecialAttackOver2(Collider col, float delay)
    {
        yield return new WaitForSeconds(delay);
        col.enabled = false;
        shieldSkillHitbox.SetActive(false);



        isAttacking = false;


    }
    void BasicAttack()
    {
        anim.SetBool("ComboOver", false);
        isAttacking = true;
        if (combocounter <= 0)
        {
            anim.SetTrigger("Attack1");
        }
        if (combocounter >= 1)
        {
            
            anim.SetTrigger("Attack2");
            anim.SetBool("isAttacking", false);

        }
        anim.SetBool("isAttacking", true);
        Collider b_collider = AttackHitbox.GetComponent<Collider>();
        //basicAttackHitbox.GetComponent<MeleeBehavior>().meleeDmg = outputDamage;


        Debug.Log("attack begins");
        combocounter++;
        mi.Velocity = 0.25f;

     

        //ComboTime();


        comboTimeout = comboMaxTime;
        //StartCoroutine(stop_BaseAttack(b_collider, activeDmgFrames));
    }
    IEnumerator stop_BaseAttack(Collider col, float delay)
    {


            yield return new WaitForSeconds(delay);
        


        col.enabled = false;
        AttackHitbox.SetActive(false);
        
        //mi.Velocity = 6;
        Debug.Log("attack stop");
        isAttacking = false;
        anim.SetBool("isAttacking", false);
        //anim.ResetTrigger("Attack1");
       // anim.ResetTrigger("Attack2");
    }
        void ShieldBlock()
    {
        mi.Velocity = 0f;
        mi.desiredRotationSpeed = 0.0f;
        //basicAttackHitbox.GetComponent<MeleeBehavior>().meleeDmg = outputDamage;
        Collider b_collider = AttackHitbox.GetComponent<Collider>();
        shieldHitbox.SetActive(true);
        b_collider.enabled = true;


     
        
 
        //StartCoroutine(stop_shield(b_collider, blockTime));
    }
   public  IEnumerator AttackHitboxTrigger()
    {
        AttackHitbox.SetActive(true);
        Collider b_collider = AttackHitbox.GetComponent<Collider>();
        b_collider.enabled = true;
       
        yield return new WaitForSeconds(0.1f);
        StartCoroutine(stop_BaseAttack(b_collider, 0));
    }
    void stop_shield(Collider col)
    {
        
        col.enabled = false;
        shieldHitbox.SetActive(false);
        mi.Velocity = 2f;
        mi.desiredRotationSpeed = 0.15f;

    }
    void ComboTime()
    {
        activeDmgFrames -= 1f * Time.deltaTime;
        if (!isAttacking && activeDmgFrames < 0.3f)
        {
            anim.SetBool("ComboOver", true);
        }
    }
    public void  DecreaseComboTime()
    {
        comboTimeout -= 1f * Time.deltaTime;
        if (comboTimeout <= 0)
        {
            combocounter = 0;
            anim.SetBool("ComboOver",true);

        }
        if (combocounter >= 1)
        {

        }
        else
        {

        }
        if (combocounter == 2)
        {
            combocounter = 0;
        }

    }
    public void shieldLifeRecharge()
    {
        if (ps.shieldlife >= 100)
            return;
        if (!isBlocking)
        {
            ps.shieldlife += shieldRechargeRate * Time.deltaTime;
        }
    }
    public void MoveSpeedRampUp()
    {
        if(mi.Velocity<=6&&!isAttacking)
        mi.Velocity += 10 * Time.deltaTime;

    }
}
