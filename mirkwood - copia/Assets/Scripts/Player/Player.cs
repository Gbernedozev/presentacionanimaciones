﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	public float inputDeadzone = 0.1f;
    public float MoveSpeed;
	public float turnSpeed = 10;
	public float strafeSpeed = 5;
    private float horizontal;
    private float vertical;
	public bool running = false;

	public LayerMask groundLayerMask;

	private Vector3 input;
	private Vector3 movement;
	private Quaternion targetRotation;
	private Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();


	}

	// Update is called once per frame
	void Update () {
        MoveInput();
        MouseInput();

	}
        public void MoveInput()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        Vector3 _movement = new Vector3(horizontal, 0, vertical);
        float x=_movement.x;
        float z=movement.z;
        ///transform.Translate(_movement*MoveSpeed*Time.deltaTime, Space.World);
        rb.velocity = (_movement * MoveSpeed);
        //Debug.Log(rb.velocity.y);
        //rb.velocity=(new Vector3(x,0,z))*MoveSpeed*Time.fixedDeltaTime;

    }
        public void MouseInput()
    {
        Ray _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit _hit;
        if (Physics.Raycast(_ray, out _hit, 1000, groundLayerMask))
        {
            transform.LookAt(new Vector3(_hit.point.x, transform.position.y, _hit.point.z));
        }
    }

 

}


