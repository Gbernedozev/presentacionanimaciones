﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackHandler : MonoBehaviour
{
    private PlayerStats ps;
    private Transform enemypos;
   // private GameObject enem;

    // Start is called before the first frame update
    void Start()
    {
        ps = GetComponentInParent<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            enemypos = other.transform;
            ApplyKnockback(other.gameObject);

            if (gameObject.tag == "AttackDefault")
                other.gameObject.GetComponent<enemy_health_manager>().life -= ps.damage;
            ps.stamina = +10;
            if (gameObject.tag == "ShieldDefault")
                other.gameObject.GetComponent<enemy_health_manager>().life -= ps.damage;
            ps.stamina = +10;
            if (gameObject.tag == "AttackSkill")
            {
                other.gameObject.GetComponent<enemy_health_manager>().life -= ps.damage * 4;
                ps.life = +ps.damage;
            }
            if (gameObject.tag == "ShieldSkill")
                other.gameObject.GetComponent<enemy_health_manager>().life -= ps.damage;
        }
    }

    
    public void ApplyKnockback(GameObject enemy)
    {
        Debug.Log("knockback niggas");
        Vector3 impulse = new Vector3((enemypos.transform.position.x - transform.position.x) * ps.weaponKnockback, 18f, 0f);

        enemy.GetComponent<Rigidbody>().isKinematic = false;
     
        enemy.GetComponent<Rigidbody>().AddForce(impulse * 5);
        ResetKnockback(enemy);


    }
    IEnumerator ResetKnockback(GameObject enem)
    {
        yield return new WaitForSeconds(0.3f);
        Debug.Log("knockback off");
        enem.GetComponent<Rigidbody>().isKinematic = true;
    }
}
