﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BifrostActivator : MonoBehaviour
{
    public GameObject abilityTree;
    public GameObject player;
    public GameObject reposition;
    float timeLeft = 30.0f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        if (abilityTree.activeSelf == true)
        {
            Debug.Log("holi");
            gameObject.SetActive(true);
            timeLeft -= Time.deltaTime;
            if (timeLeft < 0)
            {
                gameObject.SetActive(false);
            }
        }
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            CharacterController comp = collision.gameObject.GetComponent<CharacterController>();
            comp.enabled = false;
            player.transform.position = reposition.transform.position;
            comp.enabled = true;

        }
    }
}
