﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestGolemNormalAttack : MonoBehaviour
{
    Vector3 original_box_scale;
   
    void Start()
    {
        original_box_scale = GetComponent<BoxCollider>().transform.localScale;
    }

    public void Enable()
    {
        GetComponent<BoxCollider>().transform.localScale = original_box_scale;
    }

    public void Disable()
    {
        GetComponent<BoxCollider>().transform.localScale = new Vector3 (0,0,0);
    }
}
