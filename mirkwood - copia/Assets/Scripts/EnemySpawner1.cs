﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner1 : MonoBehaviour
{
    Vector3[] reposition = new [] {new Vector3(44.5f,0,59), new Vector3(22.7f,4.5f,72.8f), new Vector3(0, 0, 0) };
    int[] enemy_quantity = new int[] {1,2,3};

    public GameObject Enemy1;
    public GameObject Player;
    float dist;
    int i;
    int i_enemies;
    // Start is called before the first frame update
    void Start()
    {
        i= -1;
        i_enemies = 0;
    }

    void SpawnEnemies(int enemy_amount)
    {
        if (enemy_amount == 0)
        {
        }
        else if (enemy_amount == 1)
        {
            Instantiate(Enemy1, transform.position, transform.rotation);
        }
        else if (enemy_amount == 2)
        {
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-4, 4), transform.position.y, transform.position.z + Random.Range(-4, 4));
            Instantiate(Enemy1, transform.position, transform.rotation);
        }
        else if (enemy_amount == 3)
        {
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-4, 4), transform.position.y, transform.position.z + Random.Range(-4, 4));
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-4, 4), transform.position.y, transform.position.z + Random.Range(-4, 4));
            Instantiate(Enemy1, transform.position, transform.rotation);
        }
        else if (enemy_amount == 3)
        {
            Instantiate(Enemy1,transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-4, 4), transform.position.y, transform.position.z + Random.Range(-4, 4));
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-4, 4), transform.position.y, transform.position.z + Random.Range(-4, 4));
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-4, 4), transform.position.y, transform.position.z + Random.Range(-4, 4));
            Instantiate(Enemy1, transform.position, transform.rotation);
        }
    }

    // Update is called once per frame
    void Update()
    {
        float dist = Vector3.Distance(Player.transform.position, transform.position);
        if (dist<35)
        {
            SpawnEnemies(enemy_quantity[i_enemies]);
            i++;            
            transform.position = reposition[i];
            i_enemies++;          
        }
    }
}
