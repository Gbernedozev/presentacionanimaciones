﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turret_rotate : MonoBehaviour
{
    //this script is meant to be in a turret, which also has to have:
    //
    //* a player asset (instance) associated
    //

    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.rotation = Quaternion.LookRotation(Vector3.Normalize(player.gameObject.transform.position - gameObject.transform.position), new Vector3(0,1,0));
    }
}
