﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turret_snitch : MonoBehaviour
{
    //this script is meant to be in a collider set to "trigger", which also has to have:
    //
    //* a turret asset (instance) associated (so it can be called)
    //

    public GameObject assigned_turret;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag=="Player")
        {
            assigned_turret.gameObject.GetComponent<turret_shoot>().get_shootin = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            assigned_turret.gameObject.GetComponent<turret_shoot>().get_shootin = false;
        }
    }
}
