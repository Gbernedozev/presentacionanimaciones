﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityButton : MonoBehaviour
{
    
    
    public bool isPress;
    public bool isHighlighted;
    public Text txt;
    Animator anim;
    public AbilityTreeManager manager;
    //public AbilityTreeManager manager;
    public PlayerStats player;
    

    
    // Start is called before the first frame update
    void Start()
    {
        isPress = false;
        anim = GetComponent<Animator>();
        txt = GetComponentInChildren<Text>();
        manager = GetComponentInParent<AbilityTreeManager>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();

    }

    // Update is called once per frame
    void Update()
    {

        SkillEleccion();
        
    }

    void SkillEleccion()
    {
        if(manager.index==0 || manager.index == 1)
        {
            if (manager.botones[0].isPress == false && manager.botones[1].isPress == false)
            {
                if(PlayerStats.altarcount>=1 &&  player.essence>=2)
                {
                    if (Input.GetKeyDown(KeyCode.Return) && isHighlighted)
                    {
                        isPress = true;
                        //anim.SetBool("isPress", isPress);
                        txt.color = Color.blue;
                        txt.fontSize = 20;
                        //De momento al no haber stats a mejorar 
                        player.essence = player.essence - 2;
                    }
                }
               
            }
        }

        if (manager.index == 2 || manager.index == 3)
        {
            if (manager.botones[2].isPress == false && manager.botones[3].isPress == false)
            {
                if (PlayerStats.altarcount >= 2 && player.essence >= 2)
                {
                    if (Input.GetKeyDown(KeyCode.Return) && isHighlighted)
                    {
                        isPress = true;
                        //anim.SetBool("isPress", isPress);
                        txt.color = Color.blue;
                        txt.fontSize = 20;
                        //De momento al no haber stats a mejorar 
                        player.essence = player.essence - 2;

                    }
                }

            }
        }

        if (manager.index == 4 || manager.index == 5)
        {
            if (manager.botones[4].isPress == false && manager.botones[5].isPress == false)
            {
                if (PlayerStats.altarcount >= 3 && player.essence >= 2)
                {
                    if (Input.GetKeyDown(KeyCode.Return) && isHighlighted)
                    {
                        isPress = true;
                        //anim.SetBool("isPress", isPress);
                        txt.color = Color.blue;
                        txt.fontSize = 20;
                        //De momento al no haber stats a mejorar 
                        player.essence = player.essence - 2;

                    }
                }

            }
        }

        if (manager.index == 6 || manager.index == 7)
        {
            if (manager.botones[6].isPress == false && manager.botones[7].isPress == false)
            {
                if (PlayerStats.altarcount >= 4 && player.essence >= 2)
                {
                    if (Input.GetKeyDown(KeyCode.Return) && isHighlighted)
                    {
                        isPress = true;
                        //anim.SetBool("isPress", isPress);
                        txt.color = Color.blue;
                        txt.fontSize = 20;
                        //De momento al no haber stats a mejorar 
                        player.essence = player.essence - 2;

                    }
                }

            }
        }
            
        if (isHighlighted)
        {
            anim.SetBool("isHighlighted", isHighlighted);
        }
        else if (!isHighlighted)
        {
            anim.SetBool("isHighlighted", isHighlighted);
        }


    }

    
}
