﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StanceCanvas : MonoBehaviour
{

    
    public PlayerStats player;
    public GameObject Defense;
    public GameObject Attack;


    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        Defense = GameObject.FindGameObjectWithTag("DefStance").GetComponent<GameObject>();

        Attack = GameObject.FindGameObjectWithTag("AttStance").GetComponent<GameObject>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && player.stance==true)
        {
            player.stance = false;
            Defense.SetActive(true);
            Attack.SetActive(false);
            
        }
        else if (Input.GetKeyDown(KeyCode.Space) && player.stance == false)
        {
            player.stance = true;
            Defense.SetActive(false);
            Attack.SetActive(true);
        }
        
    }
}
