﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiManagerController : MonoBehaviour
{
    //Mecanicas de Pausar:
    public GameObject menuPausaUI;
    public static bool Pausar;


    // Start is called before the first frame update
    void Start()
    {

        Pausar = false;
        Resume();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Pausar)
            {

                Resume();
            }
            else
            {

                Pause();
            }

        }
    }

    void Pause()
    {
        menuPausaUI.SetActive(true);
        Time.timeScale = 0f;
        //FindObjectOfType<SoundManager>().Play("Pausa");
        Pausar = true;
        
    }
    void Resume()
    {

        Pausar = false;

        menuPausaUI.SetActive(false);
        Time.timeScale = 1f;
        
    }
}
