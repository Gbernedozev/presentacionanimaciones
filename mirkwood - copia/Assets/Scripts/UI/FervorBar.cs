﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FervorBar : MonoBehaviour
{
    public PlayerStats player;
    public GameObject[] barras;
    Image bar1;
    Image bar2;
    [SerializeField]
    private float lerpSpeed = 2;
    // Start is called before the first frame update
    void Start()
    {
        bar1 = barras[0].GetComponent<Image>();
        bar2 = barras[1].GetComponent<Image>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
        
        UpdateFervor();
    }

    void UpdateFervor()
    {

            Debug.Log(player.stamina/player.maxstamina);
            bar1.fillAmount = Mathf.Lerp(bar2.fillAmount, player.stamina /player.maxstamina, Time.deltaTime * lerpSpeed);
        
        /*
        else if (player.stamina<=50)
        {
            
            bar1.fillAmount = Mathf.Lerp(bar2.fillAmount, currentStamina /max, Time.deltaTime * lerpSpeed);
        }
        */
    }
}
