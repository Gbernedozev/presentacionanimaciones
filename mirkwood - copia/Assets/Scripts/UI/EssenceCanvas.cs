﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EssenceCanvas : MonoBehaviour
{
    public Text score;
    
    public PlayerStats player;

    // Start is called before the first frame update
    void Start()
    {
        score = GetComponentInChildren<Text>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
       
    }

    // Update is called once per frame
    void Update()
    {
        UpdateScore();
    }
    void UpdateScore()
    {
        score.text = "esencia :" + player.essence;

    }
}
