﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsManager : MonoBehaviour
{
    public MenuButton[] buttons;
    public int index = 0;
    // Start is called before the first frame update
    void Start()
    {
        buttons[index].isHighlighted = true;
        //ocultar cursor del mouse:
        //Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {


        
        
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            buttons[index].isHighlighted = false;

            index--;

            if (index < 0)
            {
                index = buttons.Length - 1;

                buttons[index].isHighlighted = true;
                return;
            }


            buttons[index].isHighlighted = true;
        }

        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            buttons[index].isHighlighted = false;

            index++;


            if (index > buttons.Length - 1)
            {
                index = 0;

                buttons[index].isHighlighted = true;
                return;
            }

            buttons[index].isHighlighted = true;
        }

        
    }
}
