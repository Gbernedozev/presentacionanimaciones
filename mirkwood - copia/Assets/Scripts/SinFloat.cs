﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinFloat : MonoBehaviour
{
    private Vector3 _startPosition;
    private float timeSinceStart;

    public float frequency = 2.2f;
    public float magnitude = 0.15f;

    // Update is called once per frame
    void Update ()
    {        
        if (Time.timeScale!=0){

            timeSinceStart += Time.deltaTime;
            _startPosition = transform.position;
            transform.position = _startPosition + new Vector3(0.0f, Mathf.Sin(timeSinceStart*frequency)*magnitude, 0.0f);

        }
    }
}
