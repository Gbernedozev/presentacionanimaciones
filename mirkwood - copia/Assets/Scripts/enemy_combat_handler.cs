﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy_combat_handler : MonoBehaviour
{
    //this goes on the attack_trigger, which marks the distance at which the enemy stops to beat the player up
    public Collider player_col;
    public string enemy_prefab_name = "early_spectr";
   
    // Start is called before the first frame update

    void Start()
    {
        player_col = GameObject.FindGameObjectWithTag("Player").GetComponent<Collider>();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            GetComponentInParent<enemy_move>().combat_mode = true; //making the enemy stop and stnd there
        }
    }

    private void OnTriggerStay(Collider col) 
    {
        if (col.tag == "Player")
        {
            player_col = col;

            switch (enemy_prefab_name)
            {
                case "early_spectr":
                    GetComponentInChildren<EarlySpectrWeaponTransformHandler>().doIattack = true;//if it is for the early spectr
                    break;

                case "forest_golem":
                    GetComponentInChildren<ForestGolemWeaponTransformHandler>().doIattack = true;//if it is for the forest golem
                    break;

                default:
                    GetComponentInChildren<EarlySpectrWeaponTransformHandler>().doIattack = true;//assume it is for the early spectr
                    break;
            }
        }
    }


    private void OnTriggerExit(Collider col)
    {
       
        switch(enemy_prefab_name)
        {
            case "early_spectr":
            GetComponentInChildren<EarlySpectrWeaponTransformHandler>().doIattack = false;//if it is for the early spectr
            break;

            case "forest_golem":
            GetComponentInChildren<ForestGolemWeaponTransformHandler>().doIattack = false;//if it is for the forest golem
            break;

            default:
            GetComponentInChildren<EarlySpectrWeaponTransformHandler>().doIattack = false;//assume it is for the early spectr
            break;
        }
            GetComponentInParent<enemy_move>().combat_mode = false; //making the enemy move again
    }

//public void attack

}
    

