﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy_health_manager : MonoBehaviour
{
    public GameObject orb;
    public float life;
    private Rigidbody rb;
    public float knockbacktime;
    Animator own_animator;
    float alpha_life; //this and the float below are used to detect changes on the enemys health
    float beta_life; //this and the float above are used to detect changes on the enemys health
    // Start is called before the first frame update
    void Start()
    {
        own_animator = GetComponentInParent<Animator>();
        rb = GetComponent<Rigidbody>();

        alpha_life = life;//detection system setup
        beta_life = life;//detection system setup
    }
    // Update is called once per frame
    void Update()
    {
        beta_life = life;//beta_life gets the most recent health state

        if (life <= 0)
        {
            Die();
        }
            if (rb.isKinematic == false)
            {
            Debug.Log("sdfsdf");
            StartCoroutine(ResetKnockback());
            }

        if (beta_life < alpha_life)//the most recent health state gets compared with a prior health state
        {
            alpha_life = beta_life;// the prior health state gets the recent health state so the detection cycle can start all over again 
            own_animator.SetBool("is_hit", true);
        }
        else
        {
            own_animator.SetBool("is_hit", false);
        }
    }

        void Die()
        {
            
            Instantiate(orb, this.gameObject.transform.position, this.gameObject.transform.rotation);
            Destroy(this.gameObject);
        }
        IEnumerator ResetKnockback()
        {
            yield return new WaitForSeconds(knockbacktime);
            Debug.Log("knockback off");
            rb.isKinematic = true;
        }
        
       
    }

