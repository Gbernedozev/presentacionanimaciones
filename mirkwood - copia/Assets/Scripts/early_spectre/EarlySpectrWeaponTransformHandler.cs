﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarlySpectrWeaponTransformHandler : MonoBehaviour
{
    public float hold_time = 1;
    float timer_time = 0;
    public bool doIattack = false;//modified externally by combat handler
    Collider player;
    float raycast_attack_multiplier = 0;// this and the variable below decide whether or not the attack collider is activated (value should be 0 or 1)
    float anim_attack_multiplier = 0;// this and the other variable decide whether or not the attack collider is activated (value should be 0 or 1)
    Animator own_animator;

    // Start is called before the first frame update
    void Start()
    {
        own_animator = GetComponentInParent<Animator>();
        //own_animator.Play("Enemy01Idle",-1);
    }
   
    // Update is called once per frame
    void Update()
    {
        //attack or else logic
        if (doIattack == true) //attack   if (doIattack == true && timer_time >= (hold_time / 2))
        {
            player = GetComponentInParent<enemy_combat_handler>().player_col;//get pretty much the players position

           
          

            own_animator.SetBool("gonna_attack", true); //begin attack animation

            if (Physics.Raycast(this.gameObject.transform.position, Vector3.Normalize(player.transform.position - this.gameObject.transform.position), 1000000, 1 << 9) == true)//is the player blocking? - i put "1 << 9" there and not just "9" because that´s how it works, apparently.
            {
                raycast_attack_multiplier = 0; // scale hitbox so it reaches the attack range 

            }
            else
            {
               raycast_attack_multiplier = 1;// scale hitbox so it reaches the attack range
       
            }

            this.gameObject.transform.localScale = new Vector3(raycast_attack_multiplier*anim_attack_multiplier, raycast_attack_multiplier*anim_attack_multiplier,raycast_attack_multiplier*anim_attack_multiplier);//set final scale for attack hitbox
        }
        else //be chill
        {
            own_animator.SetBool("gonna_attack", false);//return to idle animation
            this.gameObject.transform.localScale = new Vector3(0, 0, 0);//scale hitbox so it doesnt affect the player
        }

        //advance timer or else logic
        if (timer_time >= hold_time)
        {
            timer_time = 0;//reset timer when it reaches max value

          
           
            //own_animator.Play("Enemy01Attackv2", -1);
        }
        else
        {
            if (doIattack == true) //this "if" prevents the timer from updating when not being used
            {
                timer_time += (Time.deltaTime) / 2;//advance timer
            }
            else
            {
                timer_time = 0; //resets timer when not in use so it waits for attacking all over again when player rejoins fight
            }

        }

       
    }

    public void AnimAttackEnableEnd()
    {
        anim_attack_multiplier = 1;
    }

    public void AnimAttackDisableEnd()
    {
        anim_attack_multiplier = 0;
    }

}
