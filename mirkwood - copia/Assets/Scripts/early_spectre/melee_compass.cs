﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class melee_compass : MonoBehaviour
{
    //required for easier swing scripting
    // public Vector3 player_pos;
    Quaternion fromTorotation;
    Vector3 compassToplayervector;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        compassToplayervector = (GetComponentInParent<enemy_combat_handler>().player_col.transform.position - this.gameObject.transform.position);
        fromTorotation = Quaternion.FromToRotation(new Vector3(1,0, 0),new Vector3( compassToplayervector.x,0,compassToplayervector.z) );
        this.gameObject.transform.rotation = fromTorotation;
    }
}
