﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy_behavior_handler : MonoBehaviour
{
       //this thing handles waht happens when the player approaches the awareness zone trigger
   
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GetComponent<chase>().is_player_near_me = true;
            gameObject.GetComponent<patrol_test>().keep_moving = true;
        }

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            GetComponent<chase>().is_player_near_me = false;
            GetComponent<chase>().GoBackToPatrol();
            gameObject.GetComponent<patrol_test>().keep_moving = false;
        }
    }
}
